package br.com.maxmilhas.Services;

import org.json.simple.JSONObject;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class SearchService {
	
	private String endpoint = "https://flight-pricing-hmg.maxmilhas.com.br/";
	private RequestSpecification request;
	private JSONObject requestParams;
	private Response response;
	
	public SearchService() {
	    RestAssured.baseURI = this.endpoint;
	    this.request = RestAssured.given();
	    this.request.header("Content-Type", "application/json");
		this.request.header("Authorization", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJtYXhtaWxoYXMuY29tLmJyIiwiaWF0IjoxNTIxNTc4MTg3LCJleHAiOjE1MjQyNTY1ODgsImF1ZCI6InRlc3RlLXFhIiwic3ViIjoidGVzdGUtcWEiLCJlbnYiOiJobWcifQ.-Q04QmHo_Hoy3rq0y3V3hhyIeqdzqTlo27rLkhAOI7s");
		this.requestParams = new JSONObject();
	}
	
	@SuppressWarnings("unchecked")
	public void setTripType(String tripType) {
		requestParams.put("tripType", tripType);
	}
	
	@SuppressWarnings("unchecked")
	public void setFrom(String from) {
		requestParams.put("from", from);
	}
	
	@SuppressWarnings("unchecked")
	public void setTo(String to) {
		requestParams.put("to", to);
	}
	
	@SuppressWarnings("unchecked")
	public void setAdults(int adults) {
		requestParams.put("adults", adults);
	}
	
	@SuppressWarnings("unchecked")
	public void setChildren(int children) {
		requestParams.put("children", children);
	}
	
	@SuppressWarnings("unchecked")
	public void setInfants(int infants) {
		requestParams.put("infants", infants);		
	}
	
	@SuppressWarnings("unchecked")
	public void setOutboundDate(String outboundDate) {
		requestParams.put("outboundDate", "2018-05-19");
	}
	
	@SuppressWarnings("unchecked")
	public void setInboundDate(String inboundDate) {
		requestParams.put("inboundDate", "2018-05-30");
	}
	
	@SuppressWarnings("unchecked")
	public void setCabin(String cabin) {
		requestParams.put("cabin", cabin);
	}
	
	public void sendPostRequest(String param) {
		request.body(requestParams.toJSONString());
		response = request.post(param);
	}	
	
	public Response getResponse() {
		return response;
	}
	
	

}
