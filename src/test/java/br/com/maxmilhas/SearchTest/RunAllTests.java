package br.com.maxmilhas.SearchTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	Search1Adult2Infantis.class,
	Search10Adults.class,
	SearchNotFoundAeroport.class,
	SearchExecutiveFly.class,
	//SearchOutboundAtualDate.class,
	//SearchSchemaValidator.class
})

public class RunAllTests {

}
