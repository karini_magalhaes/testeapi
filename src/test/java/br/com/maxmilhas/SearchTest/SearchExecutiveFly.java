package br.com.maxmilhas.SearchTest;

import org.junit.Assert;
import org.junit.Test;

import br.com.maxmilhas.Services.SearchService;
import io.restassured.path.json.JsonPath;

public class SearchExecutiveFly {
	
	private SearchService searchService = new SearchService();
	
	@Test
	public void searchExecutiveFlyTest() {
		
		searchService.setTripType("RT");
		searchService.setFrom("CNF");
		searchService.setTo("GRU");
		searchService.setAdults(1);
		searchService.setChildren(0);
		searchService.setInfants(0);
		searchService.setOutboundDate("2018-11-01");
		searchService.setInboundDate("2018-11-21");
		searchService.setCabin("EX");
		
		searchService.sendPostRequest("/search");
		
		Assert.assertEquals(422, searchService.getResponse().getStatusCode());
		String mensagem = JsonPath.from(searchService.getResponse().asString()).getString("airlines.findAll {it.label=='avianca'}.status.message");
		//Assert.assertEquals("[Indisponível para classe executiva]", mensagem);
		
	}
}
