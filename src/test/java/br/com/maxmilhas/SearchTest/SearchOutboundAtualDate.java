package br.com.maxmilhas.SearchTest;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.junit.Assert;
import org.junit.Test;

import br.com.maxmilhas.Services.SearchService;
import io.restassured.path.json.JsonPath;

public class SearchOutboundAtualDate {
	
	private SearchService searchService = new SearchService();
	private LocalDate data = LocalDate.now();
	private DateTimeFormatter formatador = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	
	@Test
	public void searchOutboundAtualDateTest() {
		
		searchService.setTripType("RT");
		searchService.setFrom("CNF");
		searchService.setTo("GRU");
		searchService.setAdults(1);
		searchService.setChildren(0);
		searchService.setInfants(0);
		searchService.setOutboundDate(data.format(formatador));
		searchService.setInboundDate(data.plusDays(2).format(formatador));
		searchService.setCabin("EC");
		
		searchService.sendPostRequest("/search");
		
		Assert.assertEquals(200, searchService.getResponse().getStatusCode());
		String mensagem = JsonPath.given(searchService.getResponse().asString()).getString("airlines.findAll {it.label=='gol'}.status.message");
		Assert.assertEquals("[Somente partidas superiores a 24 horas]", mensagem);
	}

}
