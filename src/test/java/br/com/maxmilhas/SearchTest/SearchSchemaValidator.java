package br.com.maxmilhas.SearchTest;

import org.junit.Assert;
import org.junit.Test;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.MatcherAssert.assertThat;

import br.com.maxmilhas.Services.SearchService;
import io.restassured.module.jsv.JsonSchemaValidator;


public class SearchSchemaValidator {
       
	private SearchService searchService = new SearchService();

	@Test
	public void searchSchemaValidatorTest() {

		searchService.setTripType("RT");
		searchService.setFrom("CNF");
		searchService.setTo("GRU");
		searchService.setAdults(1);
		searchService.setChildren(0);
		searchService.setInfants(0);
		searchService.setOutboundDate("2018-11-01");
		searchService.setInboundDate("2018-11-21");
		searchService.setCabin("EC");
		
		searchService.sendPostRequest("/search");
		
		String fileJsonSchema = System.getProperty("user.dir") + System.getProperty("file.separator") + "Schemas" + System.getProperty("file.separator") + "schemaSearch.json";
		assertThat(searchService.getResponse().asString(), JsonSchemaValidator.matchesJsonSchema(fileJsonSchema));
	
	}
	
	
}
