package br.com.maxmilhas.SearchTest;

import org.junit.Assert;
import org.junit.Test;

import br.com.maxmilhas.Services.SearchService;

public class SearchNotFoundAeroport {
	
	private SearchService searchService = new SearchService();
	
	@Test
	public void searchNotFoundAeroportTest(){
		
		searchService.setTripType("RT");
		searchService.setFrom("BBB");
		searchService.setTo("GRU");
		searchService.setAdults(1);
		searchService.setChildren(0);
		searchService.setInfants(0);
		searchService.setOutboundDate("2018-11-01");
		searchService.setInboundDate("2018-11-21");
		searchService.setCabin("EC");
		
		searchService.sendPostRequest("/search");
		
		Assert.assertEquals(422, searchService.getResponse().getStatusCode());
		//Assert.assertEquals("ResourceNotFoundError", searchService.getResponse().jsonPath().get("type"));
		//Assert.assertEquals("Could not create search resource due to some incorrect parameter.", searchService.getResponse().jsonPath().get("message"));
	}
}
