package br.com.maxmilhas.SearchTest;

import org.junit.Assert;
import org.junit.Test;

import br.com.maxmilhas.Services.SearchService;

public class Search10Adults {
	
	private SearchService searchService = new SearchService();
	
	@Test
	public void search10AdultsTest() {
		
		searchService.setTripType("RT");
		searchService.setFrom("CNF");
		searchService.setTo("GRU");
		searchService.setAdults(10);
		searchService.setChildren(0);
		searchService.setInfants(0);
		searchService.setOutboundDate("2018-11-01");
		searchService.setInboundDate("2018-11-21");
		searchService.setCabin("EC");
		
		searchService.sendPostRequest("/search");
		
		Assert.assertEquals(422, searchService.getResponse().getStatusCode());
		Assert.assertEquals("PassengersQuantityValidationError", searchService.getResponse().jsonPath().get("type"));
		Assert.assertEquals("The total number of passengers exceeds the maximum 9 allowed", searchService.getResponse().jsonPath().get("message"));
	}
}
