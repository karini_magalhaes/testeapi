package br.com.maxmilhas.SearchTest;

import org.junit.Assert;
import org.junit.Test;

import br.com.maxmilhas.Services.SearchService;

public class Search1Adult2Infantis {
	
	private SearchService searchService = new SearchService();
	
	@Test
	public void search1Adult2InfantisTest() {
		
		searchService.setTripType("RT");
		searchService.setFrom("CNF");
		searchService.setTo("GRU");
		searchService.setAdults(1);
		searchService.setChildren(0);
		searchService.setInfants(2);
		searchService.setOutboundDate("2018-11-01");
		searchService.setInboundDate("2018-11-21");
		searchService.setCabin("EC");
		
		searchService.sendPostRequest("/search");
				
		Assert.assertEquals(422, searchService.getResponse().getStatusCode());
		Assert.assertEquals("AdultsGreaterInfantsValidationError", searchService.getResponse().jsonPath().get("type"));
		Assert.assertEquals("The number of adults must be greater than that of infants", searchService.getResponse().jsonPath().get("message"));
	}
}
