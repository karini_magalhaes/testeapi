# Desafio Técnico MaxMilhas


Repositório para o projeto de testes automatizados do processo de seleção MaxMilhas.

### Arquitetura ###

Stack de Ferramentas:

* IDE: Eclipse Oxigen 4.7.0
* Linguagem: Java
* Gerenciamento de Dependências: Maven 3.5.0
* Test Framework: Rest Assured 3.1.0
* Test Run: JUnit version 4.12

Estrutura:

* src/main/java: armazenam os pacotes e classes dos serviços.
* src/main/test: armazenam os pacotes e classes de teste. Cada classe possui um único cenário de teste.

Execução:

Os testes podem ser executados via maven (mvn clean test -Dtests="RunAllTests") ou via Junit acessando br.com.maxmilhas.SearchTests > RunAllTests.
OBS: Necessário instalação e configuração do maven para executa-lo via linha de comando.